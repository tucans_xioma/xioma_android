package com.tucans.adsserviceadvisor;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class JobCardActivity extends Activity implements GetJSON {

	JobGeneralFragment jgFragment = null;
	JobCardActivity activity;
	DataComm dc = null;
	String docEntry = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Resources res = getResources();
		activity = this;
		dc = new DataComm(this);

		setContentView(R.layout.activity_job_tabs_fragment_layout);
		Util.closeKeyboard(this);
		Bundle b = getIntent().getExtras();

		TextView tv = (TextView) findViewById(R.id.job_card_num_title);
		tv.setText(res.getString(R.string.job_card_prefix) + b.getString(ADSConstants.JOB_CARD_NUM_EXTRA) + ":");
		tv = (TextView) findViewById(R.id.job_card_status_title);
		tv.setText(b.getString(ADSConstants.JOB_STATUS_EXTRA));
		tv = (TextView) findViewById(R.id.job_card_model_title);
		tv.setText(b.getString(ADSConstants.JOB_MODEL_EXTRA));
		tv = (TextView) findViewById(R.id.job_card_registration_title);
		tv.setText(res.getString(R.string.reg_abbrev) + " " + b.getString(ADSConstants.JOB_REGISTRATION_EXTRA));
		docEntry = b.getString(ADSConstants.JOB_DOC_ENTRY_EXTRA);
		if (savedInstanceState == null) {
			try {
				LatLng place = Util.GetLocationCoord();
				dc.execute(
						String.format("GetGeneralData/%s,%s,%s,%s",
								Settings.GetStringSetting(activity, Settings.SESSION_ID_KEY),
								place.latitude, place.longitude, docEntry));
				jgFragment = new JobGeneralFragment();
				getFragmentManager().beginTransaction()
				.add(R.id.jobtabcontent, jgFragment).commit();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void ShowGeneral(View v) {
		if (jgFragment != null)
			getFragmentManager().beginTransaction()
			.add(R.id.jobtabcontent, jgFragment).commit();
	}

	@Override
	public void jsonStart() {

	}

	@Override
	public void parseJSON(JSONObject jsonObject) {

		try {
			if (jsonObject.has("GetGeneralDataResult")) {
				LoadGeneral(jsonObject.getJSONObject("GetGeneralDataResult"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void LoadGeneral(JSONObject generalTabData) throws JSONException {

		jgFragment.SetDetails(generalTabData);
		
	}

	@Override
	public void onError(String errorMessage) {
		// TODO Auto-generated method stub

	}
	
	public void DoSave(View v) {
		jgFragment.DoSave();
	}
	
	public void ToggleEdit(View v) {
		jgFragment.ToggleEdit();
	}
	
	public void GoToVehicleCard(View v) {
		jgFragment.GoToVehicleCard();
	}

}
