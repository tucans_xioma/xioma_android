package com.tucans.adsserviceadvisor;

import org.json.JSONObject;

public interface GetJSON {
	public void jsonStart();
	public void parseJSON(JSONObject jsonObject);
	public void onError(String errorMessage);
}
