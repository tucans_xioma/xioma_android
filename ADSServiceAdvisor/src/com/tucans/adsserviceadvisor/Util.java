package com.tucans.adsserviceadvisor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.net.ParseException;
import android.view.inputmethod.InputMethodManager;


public class Util {

	static public void closeKeyboard(Activity activity) {
		InputMethodManager inputManager = (InputMethodManager)            
				activity.getSystemService(Context.INPUT_METHOD_SERVICE); 
		if (activity.getCurrentFocus() != null) {
			inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),      
					InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	public static CharSequence FormatDate(Date date) {
		String outDateString = null;
		SimpleDateFormat outSDF = new SimpleDateFormat("dd/MM/yy", Locale.US);
		try {
			outDateString = outSDF.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			
		}
		if (outDateString == null)
			outDateString = "------";
		return outDateString;
	}
	
	public static LatLng GetLocationCoord() {
		return new LatLng(0,0);
	}
}
