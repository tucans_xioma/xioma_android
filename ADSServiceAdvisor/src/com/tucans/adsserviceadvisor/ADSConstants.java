package com.tucans.adsserviceadvisor;

public class ADSConstants {
	public static final String JOB_TAB_GENERAL  = "job_tab_general_id";
	public static final String JOB_TAB_REQUESTS  = "job_tab_requests_id";
	public static final String JOB_TAB_TRACK = "job_tab_track_id";
	public static final String JOB_TAB_CONDITION = "job_tab_vehicle_condition_id";
	public static final String JOB_TAB_TIMEKEEPER = "job_tab_timekeeper_id";
	
	public static final String JOB_CARD_NUM_EXTRA = "job_card_num_extra";
	public static final String JOB_STATUS_EXTRA = "job_status_extra";
	public static final String JOB_MODEL_EXTRA = "job_car_type_extra";
	public static final String JOB_REGISTRATION_EXTRA = "job_registration_extra";
	public static final String JOB_DOC_ENTRY_EXTRA = "job_doc_entry_extra";
}
