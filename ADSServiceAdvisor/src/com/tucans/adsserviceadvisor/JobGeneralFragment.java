package com.tucans.adsserviceadvisor;

import java.util.Date;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

// TODO: load spinners - status, consultant, fuel tank
// TODO: implement button functions

public class JobGeneralFragment extends Fragment {

	String DocEntry;
	String DocNum;
	String StatusCode;
	String CardName;
	String StatusDescription;
	String LicenseNumber;
	String VIN;
	String SecurityCode;
	String ManufacturerCode;
	String ManufacturerDesc;
	String ModelCode;
	String ModelDescription;
	String VehicleModel;
	String ModelYear;
	String Owner;
	String OwnerPhone;
	String ID;
	String Driver;
	String DriverId;
	String ConsultantCode;
	String ConsultantName;
	String Odometer;
	String FuelTank;
	String FuelTankCode;
	Date OpenDate;
	Date EntryDate;
	Date CloseDate;
	Date DueDate;
	String CustomerNotes;
	String Notes;
	String VehicleNotes;
	String SeriesName;
	String Series;
	
	JSONObject parseObject;
	View containerView;
	boolean isEditing = false;
	
	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	        containerView = inflater.inflate(R.layout.fragment_jobs_general_layout, container, false);
	        SetEditingEnabled(isEditing);
	        return containerView;
	    }

		public void SetDetails(JSONObject jsonObject) {
			// activate keys
			try {
				parseObject = jsonObject.getJSONObject("cardDetails");
				if (parseObject != null) {
					DocEntry = ParseObject("DocEntry");
					DocNum = ParseObject("DocNum");
					StatusCode = ParseObject("StatusCode");
					CardName = ParseObject("CardName");
					StatusDescription = ParseObject("StatusDescription");
					LicenseNumber = ParseObject("LicenseNumber");
					VIN = ParseObject("VIN");
					SecurityCode = ParseObject("SecurityCode");
					ManufacturerCode = ParseObject("ManufacturerCode");
					ManufacturerDesc = ParseObject("ManufacturerDesc");
					ModelCode = ParseObject("ModelCode");
					ModelDescription = ParseObject("ModelDescription");
					VehicleModel = ParseObject("VeihcleModel");
					ModelYear = ParseObject("ModelYear");
					Owner = ParseObject("Owner");
					OwnerPhone = ParseObject("OwnerPhone");
					ID = ParseObject("ID");
					Driver = ParseObject("Driver");
					DriverId = ParseObject("DriverId");
					ConsultantCode = ParseObject("ConsultantCode");
					ConsultantName = ParseObject("ConsultantName");
					Odometer = ParseObject("Odometer");
					FuelTank = ParseObject("FuelTank");
					FuelTankCode = ParseObject("FuelTankCode");
					OpenDate = ParseDate("OpenDate");
					EntryDate = ParseDate("EntryDate");
					CloseDate = ParseDate("CloseDate");
					DueDate = ParseDate("DueDate");
					CustomerNotes = ParseObject("CustomerNotes");
					Notes = ParseObject("Notes");
					VehicleNotes = ParseObject("VehicleNotes");
					SeriesName = ParseObject("SeriesName");
					Series = ParseObject("Series");
					
				}
				if (jsonObject.has("fuelTankTypes"))
					LoadFuelTankSpinner(jsonObject.getJSONArray("fuelTankTypes"));
				if (jsonObject.has("statusDescriptions"))
					LoadStatusSpinner(jsonObject.getJSONArray("statusDescriptions"));
				if (jsonObject.has("consultantNames"))
					LoadConsultantSpinner(jsonObject.getJSONArray("consultantNames"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			LoadViews();
			isEditing = false;
			SetEditingEnabled(isEditing);
			EnableButtons(true);
		}

		private void EnableButtons(boolean isEnabled) {
			ImageButton ib = (ImageButton) containerView.findViewById(R.id.save_button);
			ib.setEnabled(isEnabled);
			ib = (ImageButton) containerView.findViewById(R.id.edit_button);
			ib.setEnabled(isEnabled);
			ib = (ImageButton) containerView.findViewById(R.id.vehicle_card_button);
			ib.setEnabled(isEnabled);
			
		}

		private void SetEditingEnabled(boolean isEnabled) {
			
			EditText et = (EditText) containerView.findViewById(R.id.et_gen_vehic_reg);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_vin_no);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_security);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_manufacturer);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_model);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_year);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_customer_name);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_driver);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_phone_number);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_cust_id);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_driver_id);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_open_date);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_entry_date);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_promise2_date);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_closing_date);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_cust_notes);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_vehicle_notes);
			et.setEnabled(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_card_notes);
			et.setEnabled(isEnabled);
			
		}

		private void LoadViews() {
			EditText et = (EditText) containerView.findViewById(R.id.et_gen_vehic_reg);
			et.setText(LicenseNumber);
			et = (EditText) containerView.findViewById(R.id.et_gen_vin_no);
			et.setText(VIN);
			et = (EditText) containerView.findViewById(R.id.et_gen_security);
			et.setText(SecurityCode);
			et = (EditText) containerView.findViewById(R.id.et_gen_manufacturer);
			et.setText(ManufacturerDesc);
			et = (EditText) containerView.findViewById(R.id.et_gen_model);
			et.setText(ModelDescription);
			et = (EditText) containerView.findViewById(R.id.et_gen_year);
			et.setText(ModelYear);
			et = (EditText) containerView.findViewById(R.id.et_gen_customer_name);
			et.setText(Owner);
			et = (EditText) containerView.findViewById(R.id.et_gen_driver);
			et.setText(Driver);
			et = (EditText) containerView.findViewById(R.id.et_gen_phone_number);
			et.setText(OwnerPhone);
			et = (EditText) containerView.findViewById(R.id.et_gen_cust_id);
			et.setText(ID);
			et = (EditText) containerView.findViewById(R.id.et_gen_driver_id);
			et.setText(DriverId);
			Spinner spinner = (Spinner) containerView.findViewById(R.id.spinner_gen_status);
			//spinner.setText(StatusDescription);
			spinner = (Spinner) containerView.findViewById(R.id.spinner_gen_consultant);
			//spinner.setText(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_odometer);
			et.setText(Odometer);
			spinner = (Spinner) containerView.findViewById(R.id.spinner_gen_fuel_tank);
			//spinner.setText(isEnabled);
			et = (EditText) containerView.findViewById(R.id.et_gen_open_date);
			et.setText(Util.FormatDate(OpenDate));
			et = (EditText) containerView.findViewById(R.id.et_gen_entry_date);
			et.setText(Util.FormatDate(EntryDate));
			et = (EditText) containerView.findViewById(R.id.et_gen_promise2_date);
			et.setText(Util.FormatDate(DueDate));
			et = (EditText) containerView.findViewById(R.id.et_gen_closing_date);
			et.setText(Util.FormatDate(CloseDate));
			et = (EditText) containerView.findViewById(R.id.et_gen_cust_notes);
			et.setText(CustomerNotes);
			et = (EditText) containerView.findViewById(R.id.et_gen_vehicle_notes);
			et.setText(VehicleNotes);
			et = (EditText) containerView.findViewById(R.id.et_gen_card_notes);
			et.setText(Notes);
		}

		private Date ParseDate(String name) throws JSONException {
			Date getDate = null;
			if (parseObject.has(name) && !parseObject.getString(name).equals(null) && !parseObject.getString(name).equals("null"))
			{
				try {
					Integer ticks = Integer.parseInt(parseObject.getString(name));
					getDate = new Date(ticks);
				} catch (NumberFormatException e) {
					// OK, so we return null
				}
			}
			return getDate;
		}

		private String ParseObject(String name) throws JSONException {
			String objectVal = null;
			if (parseObject.has(name) && !parseObject.getString(name).equals(null) && !parseObject.getString(name).equals("null")) {
				objectVal = parseObject.getString(name);
			}
			return objectVal;
		}

		public void LoadFuelTankSpinner(JSONArray tankArray) throws JSONException {
			Vector<String> tanksList = new Vector<String>();
			if (tankArray == null) {
				tanksList.add("Tank 1");
				tanksList.add("Tank 2");
			} else {
				for (int i = 0; i < tankArray.length(); i++) {
					tanksList.add(tankArray.get(i).toString());
				}
			}

			Spinner spinner = (Spinner) containerView.findViewById(R.id.spinner_gen_fuel_tank);
			// Create an ArrayAdapter using the string array and a default spinner layout
			ArrayAdapter adapter = new ArrayAdapter(getActivity().getBaseContext(),
					android.R.layout.simple_spinner_dropdown_item,
					tanksList);
			// Specify the layout to use when the list of choices appears
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			// Apply the adapter to the spinner
			spinner.setAdapter(adapter);
			spinner.setSelection(0); // TODO: set to tank 

		}
		

		private void LoadConsultantSpinner(JSONArray consArray) throws JSONException {
			Vector<String> consultantsList = new Vector<String>();
			
			if (consArray != null) {
				for (int i = 0; i < consArray.length(); i++) {
					consultantsList.add(consArray.get(i).toString());
				}
				Spinner spinner = (Spinner) containerView.findViewById(R.id.spinner_gen_fuel_tank);
				// Create an ArrayAdapter using the string array and a default spinner layout
				ArrayAdapter adapter = new ArrayAdapter(getActivity().getBaseContext(),
						android.R.layout.simple_spinner_dropdown_item,
						consultantsList);
				// Specify the layout to use when the list of choices appears
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				// Apply the adapter to the spinner
				spinner.setAdapter(adapter);
				spinner.setSelection(0); // TODO: set to consultant name
				
			}
		}

		private void LoadStatusSpinner(JSONArray statusArray) throws JSONException {
			Vector<String> statusList = new Vector<String>();
			
			if (statusArray != null) {
				for (int i = 0; i < statusArray.length(); i++) {
					statusList.add(statusArray.get(i).toString());
				}
				Spinner spinner = (Spinner) containerView.findViewById(R.id.spinner_gen_fuel_tank);
				// Create an ArrayAdapter using the string array and a default spinner layout
				ArrayAdapter adapter = new ArrayAdapter(getActivity().getBaseContext(),
						android.R.layout.simple_spinner_dropdown_item,
						statusList);
				// Specify the layout to use when the list of choices appears
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				// Apply the adapter to the spinner
				spinner.setAdapter(adapter);
				spinner.setSelection(0); // TODO: set to consultant name
				
			}
		}

		public void DoSave() {
			// TODO Auto-generated method stub
			
		}

		public void ToggleEdit() {
			isEditing = !isEditing;
			SetEditingEnabled(isEditing);
		}

		public void GoToVehicleCard() {
			// TODO Auto-generated method stub
			
		}


}
