package com.tucans.adsserviceadvisor;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class JobCardMainActivity extends Activity implements OnItemSelectedListener, GetJSON {
	private final String NULL_STRING = "null";
	JobCardMainActivity jcmActivity;
	class JobCardRow {
		public String docEntry;
		public String docnum;
		public String statusCode;
		public String statusDesc;
		public String entryDate;
		public String entryHour; 
		public String description;
		public String registration;
		public String model;
	}
	ArrayList<JobCardRow> jobsList;
	ArrayList<JobCardRow> filteredJobsList;
	
	OnClickListener listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(jcmActivity, JobCardActivity.class);
			TextView tv = (TextView) v.findViewById(R.id.job_row_number);
			intent.putExtra(ADSConstants.JOB_CARD_NUM_EXTRA, tv.getText().toString());
			intent.putExtra(ADSConstants.JOB_DOC_ENTRY_EXTRA, getDocEntry(tv.getText().toString()));
			tv = (TextView) v.findViewById(R.id.job_status);
			intent.putExtra(ADSConstants.JOB_STATUS_EXTRA, tv.getText().toString());
			tv = (TextView) v.findViewById(R.id.job_model);
			intent.putExtra(ADSConstants.JOB_MODEL_EXTRA, tv.getText().toString());
			tv = (TextView) v.findViewById(R.id.job_registration);
			intent.putExtra(ADSConstants.JOB_REGISTRATION_EXTRA, tv.getText().toString());
			jcmActivity.startActivity(intent);
		}

		private String getDocEntry(String docNum) {
			String docEntry = null;
			for (JobCardRow jcr : filteredJobsList) {
				if (jcr.docnum.equals(docNum)) {
					docEntry = jcr.docEntry;
					break;
				}
			}
			return docEntry;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		jcmActivity = this;
		jobsList = new ArrayList<JobCardMainActivity.JobCardRow>();
		filteredJobsList = new ArrayList<JobCardMainActivity.JobCardRow>();
		try {
			setContentView(R.layout.activity_job_cards);

			Spinner spinner = (Spinner) findViewById(R.id.filter_spinner);
			// Create an ArrayAdapter using the string array and a default spinner layout
			ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
					R.array.job_filter_array, R.layout.spinner_item);
			// Specify the layout to use when the list of choices appears
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			// Apply the adapter to the spinner
			spinner.setAdapter(adapter);
			spinner.setSelection(0);
			TextView vtv = (TextView) findViewById(R.id.textViewJobcardTitle);
			vtv.setText(getResources().getString(R.string.job_cards));

			// Get job card table
			String sessionID = Settings.GetStringSetting(this, Settings.SESSION_ID_KEY);
			DataComm dc = new DataComm(this);
			String statusFilter = "3";
			String latitude = "0";
			String longitude = "0";
			String searchString = "null";
			dc.execute(String.format("GetJobCardList/%s,%s,%s,%s,%s", sessionID, latitude, longitude, statusFilter, searchString));
			
		} catch (Exception e) {
			String errMsg = e.getMessage();
			if (errMsg == null)
				e.printStackTrace();
			else
				Log.e(getClass().getName(), errMsg);
		}

		/*if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}*/
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		filteredJobsList.clear();
		Spinner spinner = (Spinner) findViewById(R.id.filter_spinner);
		String filter = spinner.getSelectedItem().toString();
		for (JobCardRow jcr : jobsList) {
			if (passesFilter(filter, jcr.statusCode))
				filteredJobsList.add(jcr);
		}
		fillTable();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	@Override
	public void jsonStart() {
		// TODO Auto-generated method stub

	}

	@Override
	public void parseJSON(JSONObject jsonObject) {
		try {
			Spinner spinner = (Spinner) findViewById(R.id.filter_spinner);
			String filter = spinner.getSelectedItem().toString();
			JSONArray ja = jsonObject.getJSONArray("GetJobCardListResult");

			Log.d(getClass().getName(), "Start Inflating and Adding Rows");
			for (int i = 0; i < ja.length(); i++) {
				JSONObject jo = ja.getJSONObject(i);
				JobCardRow jcr = new JobCardRow();
				jcr.docEntry = jo.getString("DocEntry");
				jcr.docnum = jo.getString("DocNum");
				jcr.statusCode = jo.getString("JobStatusCode");
				jcr.statusDesc = jo.getString("JobStatusDesc");
				jcr.entryDate = jo.getString("EntryDate"); // TODO: localize
				if (jcr.entryDate.toLowerCase().equals(NULL_STRING))
					jcr.entryDate = "";
				jcr.entryHour = ""; //jo.getString("U_EntryHr"); 
				jcr.description = jo.getString("Requests");
				jcr.registration = jo.getString("LicenseNumber");
				jcr.model = jo.getString("VeihcleModel");
				jobsList.add(jcr);
				if (passesFilter(filter, jcr.statusCode))
					filteredJobsList.add(jcr);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception ex) {
			if (ex.getMessage() == null)
				ex.printStackTrace();
			else
				Log.e(getClass().getName(), ex.getMessage());
		}
		fillTable();
	}


	private boolean passesFilter(String filter, String statusCode) {
		boolean isPass = true;
		
		if (filter.toLowerCase().equals("all")) {
			isPass = true;
		} else if (filter.toLowerCase().equals("open")) {
			isPass = statusCode.equals("1") || statusCode.equals("2");
		} else if (filter.toLowerCase().equals("my open")){ // my open
			isPass = statusCode.equals("1");
		}
		return isPass;
	}

	void fillTable()
	{
		TableLayout parent = (TableLayout) findViewById(R.id.job_cards_table);
		parent.removeAllViews();

		for (JobCardRow jcr : filteredJobsList) {

			LayoutInflater inflater = (LayoutInflater) 
					getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			TableRow rowView = (TableRow) inflater.inflate(R.layout.job_card_row, parent, false);
			TextView tv = (TextView) rowView.findViewById(R.id.job_row_number);
			tv.setText(jcr.docnum);
			tv = (TextView) rowView.findViewById(R.id.job_status);
			tv.setText(jcr.statusDesc);
			tv = (TextView) rowView.findViewById(R.id.job_date);
			tv.setText(String.format("%s %s", jcr.entryDate, jcr.entryHour));
			tv = (TextView) rowView.findViewById(R.id.job_description);
			tv.setText(jcr.description);
			tv = (TextView) rowView.findViewById(R.id.job_registration);
			tv.setText(jcr.registration);
			tv = (TextView) rowView.findViewById(R.id.job_model);
			tv.setText(jcr.model);
			rowView.setOnClickListener(listener);
			parent.addView(rowView);
		}
		
	}

	@Override
	public void onError(String errorMessage) {
		// TODO Auto-generated method stub

	}

}
