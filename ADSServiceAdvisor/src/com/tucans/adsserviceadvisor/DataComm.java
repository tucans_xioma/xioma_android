package com.tucans.adsserviceadvisor;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;

public class DataComm extends AsyncTask<String, Void, JSONObject> {

	GetJSON activity;
	private String baseURL = "http://10.0.0.8:4567/ADSService.svc/";//?q=p_services/system/";
	String errorMessage = "No Error";
	ProgressDialog pd = null;
	Context context;
	public DataComm(GetJSON _activity) {
		activity = _activity;
	}

	@Override
	protected void onPreExecute() {
		context = (Context) activity;
		pd = ProgressDialog.show(context, context.getResources().getText(R.string.loading), "", true);
		activity.jsonStart();
	}

	@Override
	protected JSONObject doInBackground(String... params) {
		HttpResponse response;
		errorMessage = "No Error";
		JSONObject jsonResponse = null;
		String address = Settings.GetStringSetting(context, Settings.REST_ADDRESS_KEY);
		String port = Settings.GetStringSetting(context, Settings.REST_PORT_KEY);
		boolean isSSL = Settings.GetBoolSetting(context, Settings.COMM_IS_SSL_KEY);
		if (isSSL)
			baseURL = "https://" + address;
		else
			baseURL = "http://" + address;
		if (!port.equals(""))
			baseURL += ":"+port;
		baseURL += "/ADSService.svc/";
	    AndroidHttpClient client = AndroidHttpClient.newInstance("Android"); 
		try {
			HttpGet request = new HttpGet(baseURL + Uri.encode(params[0]));
		    response = client.execute(request);
			String responseStr = EntityUtils.toString(response.getEntity());
			jsonResponse = new JSONObject(responseStr);

		} catch (ClientProtocolException e) {
			if (e.getMessage() != null)
				errorMessage = e.getMessage();
			response = null;
			e.printStackTrace();
		} catch (IOException e) {
			if (e.getMessage() != null)
				errorMessage = e.getMessage();
			e.printStackTrace();
			response = null;
		} catch (Exception e) {
			if (e.getMessage() != null)
				errorMessage = e.getMessage();
			response = null;
		} finally {
			client.close();
		}
		
		return jsonResponse;
	}

	@Override
	protected void onPostExecute(JSONObject result) {

		super.onPostExecute(result);       

		if (pd.isShowing())
			pd.cancel();
		if (result == null || !errorMessage.equals("No Error")) {
			activity.onError(errorMessage);
		} else {
			activity.parseJSON(result);					
		}
	}

}
