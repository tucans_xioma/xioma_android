package com.tucans.adsserviceadvisor;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Build;

public class SettingsActivity extends Activity {
	public static final String PREFS_NAME = "ADSPrefsFile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		// get and display device ID
		TextView tv = (TextView) findViewById(R.id.textViewDeviceIDValue);
		tv.setText(Settings.GetDeviceID(this));
		String address = Settings.GetStringSetting(this, Settings.REST_ADDRESS_KEY);
		String port = Settings.GetStringSetting(this, Settings.REST_PORT_KEY);
		boolean isSSL = Settings.GetBoolSetting(this, Settings.COMM_IS_SSL_KEY);
		String dbName = Settings.GetStringSetting(this, Settings.DB_NAME);
		EditText etv = (EditText) findViewById(R.id.editTextServer);
		etv.setText(address);
		etv = (EditText) findViewById(R.id.editTextPort);
		etv.setText(port);
		CheckBox cb = (CheckBox) findViewById(R.id.checkBoxSSL);
		cb.setChecked(isSSL);
		etv = (EditText) findViewById(R.id.EditTextDBname);
		etv.setText(dbName);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void DoSave(View v) {
		EditText ev = (EditText) findViewById(R.id.editTextServer);
		String serverAddress = ev.getText().toString();
		ev = (EditText) findViewById(R.id.editTextPort);
		String portNum = ev.getText().toString();
		CheckBox cb = (CheckBox) findViewById(R.id.checkBoxSSL);
		Boolean isSSL = cb.isChecked();
		ev = (EditText) findViewById(R.id.EditTextDBname);
		String dbName = ev.getText().toString();
		// Check address and port validity
		try {
			if (!portNum.matches("[0-9]+") && !portNum.equals("")) 
				throw new Exception ("Invalid Port Number");
			String validIpAddressRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
			String validHostnameRegex = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$";
			if ((serverAddress.matches(validHostnameRegex) || serverAddress.matches(validIpAddressRegex))) {
				Settings.SetStringSetting(this, Settings.REST_ADDRESS_KEY, serverAddress);
				Settings.SetStringSetting(this, Settings.REST_PORT_KEY, portNum);
				Settings.SetBoolean(this, Settings.COMM_IS_SSL_KEY, isSSL);
				Settings.SetStringSetting(this, Settings.DB_NAME, dbName);
				return2login();
				
			} else throw new Exception ("Invalid Server Address");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void DoCancel(View v) {
		return2login();
	}
	
	void return2login() {
		Intent i = new Intent(this, LoginActivity.class);
		startActivity(i);
		this.finish();
	}


}
