package com.tucans.adsserviceadvisor;

import org.json.JSONException;
import org.json.JSONObject;

import android.support.v4.app.Fragment;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.os.Build;

public class LoginActivity extends Activity implements GetJSON {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_login,
					container, false);
			return rootView;
		}
	}
	
	public void DoLogin(View v) {
		DataComm dc = new DataComm(this);
		EditText et = (EditText) findViewById(R.id.user_code_edit);
		String user = et.getText().toString();
		et = (EditText) findViewById(R.id.password_edit);
		String password = et.getText().toString();
		String deviceID = "4444";
		String deviceType = "Samsung note 9";
		String OStype = "Android";
		String OSVersion = "4.2.2";
		String latitude = "null";
		String longitude = "null";
		dc.execute(String.format("NewSession/%s,%s,%s,%s,%s,%s,%s,%s",deviceID, user, password, deviceType, OStype, OSVersion, latitude, longitude));
	}

	@Override
	public void jsonStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void parseJSON(JSONObject jsonObject) {
		try {
			Settings.SetStringSetting(this, Settings.SESSION_ID_KEY, jsonObject.getString("NewSessionResult"));
			EditText et = (EditText) findViewById(R.id.user_code_edit);
			Settings.SetStringSetting(this, Settings.USER_NAME_KEY, et.getText().toString());
			Intent i = new Intent(this, MainActivity.class);
			startActivity(i);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}

	@Override
	public void onError(String errorMessage) {
		// TODO Auto-generated method stub
		
	}

}
