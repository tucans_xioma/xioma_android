package com.tucans.adsserviceadvisor;

public class LatLng {
	float latitude;
	float longitude;
	public LatLng(float _lat, float _lng){
		latitude = _lat;
		longitude = _lng;
	}
}
