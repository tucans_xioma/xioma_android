package com.tucans.adsserviceadvisor;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

public class Settings {
	static final String PREFS_NAME = "ADSPrefsFile";
	public static final String SESSION_ID_KEY = "session_id";
	public static final String USER_NAME_KEY = "user_name";
	public static final String REST_ADDRESS_KEY = "address_key";
	public static final String REST_PORT_KEY = "rest_port_key";
	public static final String COMM_IS_SSL_KEY = "comm_is_ssl_key";
	public static final String DB_NAME = "db_name";
	
	static String GetStringSetting(Context context, String key) {
		String result = "";
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		result = settings.getString(key, "");
		return result;
	}
	
	static void SetStringSetting(Context context, String key, String value) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	static boolean GetBoolSetting(Context context, String key) {
		boolean result = false;
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		result = settings.getBoolean(key, false);
		return result;
	}
	
	static void SetBoolean(Context context, String key, boolean value){
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	static String GetDeviceID(Context context) {
		String ts = Context.TELEPHONY_SERVICE;
		TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(ts);
		return mTelephonyMgr.getDeviceId();
	}
	
}
